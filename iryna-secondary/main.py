import time
import asyncio
import json
from contextlib import suppress
from datetime import datetime

import grpc
from aiohttp import web
from grpc.experimental.aio import init_grpc_aio
from grpc_health.v1 import health
from grpc_health.v1 import health_pb2
from grpc_health.v1 import health_pb2_grpc

from message_pb2 import AppendMessageReply
import message_pb2
from message_pb2_grpc import add_MessageReplicationServicer_to_server, MessageReplicationServicer

import logging

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG)

message_list = []


class MessageView(web.View):
    async def get(self) -> web.Response:
        return web.Response(body=json.dumps(message_list.sort(key=lambda k: k['id'])))


class Application(web.Application):
    def __init__(self):
        super().__init__()
        self.add_routes()

    def add_routes(self):
        self.router.add_view('/messages', MessageView)

    def run(self):
        return web.run_app(self, port=8000)


class MessageServicer(MessageReplicationServicer):
    def AppendMessage(self, request, context):
        dt = datetime.fromtimestamp(request.created_at.seconds + request.created_at.nanos / 1e9)
        tss = dt.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        messages = {"id": request.id, "messageText": request.message_text,
                    "createdAt": tss}
        logging.info('Received messages from master')
        if messages not in message_list:
            message_list.append(messages)
        time.sleep(2)

        response = AppendMessageReply()
        response.success = True
        response.response_text = "Replicated successfully"
        logging.info('Replicated successfully')
        return response


class GrpcServer:
    def __init__(self):
        init_grpc_aio()
        self.server = grpc.experimental.aio.server()
        self.servicer = MessageServicer()
        self.health_servicer = health.HealthServicer()
        add_MessageReplicationServicer_to_server(
            self.servicer,
            self.server)
        health_pb2_grpc.add_HealthServicer_to_server(
            self.health_servicer, self.server)
        services = tuple(
            service.full_name
            for service in message_pb2.DESCRIPTOR.services_by_name.values()) + (
                    health.SERVICE_NAME,)
        for service in services:
            self.health_servicer.set(service, health_pb2.HealthCheckResponse.SERVING)
        self.server.add_insecure_port("[::]:50051")

    async def start(self):
        logging.info('gRPC server started')
        await self.server.start()
        await self.server.wait_for_termination()

    async def stop(self):
        logging.info('gRPC server stopped')
        await self.server.close()
        await self.server.wait_for_termination()


async def run_grpc_server(app):
    try:
        server = GrpcServer()
        await server.start()
    except asyncio.CancelledError:
        pass
    finally:
        await server.stop()


async def subtask_wrapped(app):
    task = asyncio.create_task(run_grpc_server(app))

    yield

    task.cancel()
    with suppress(asyncio.CancelledError):
        await task  # Ensure any exceptions etc. are raised.


application = Application()

if __name__ == '__main__':
    application.cleanup_ctx.append(subtask_wrapped)
    application.run()
